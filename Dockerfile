FROM registry.access.redhat.com/ubi9/ubi-minimal:latest@sha256:14f14e03d68f7fd5f2b18a13478b6b127c341b346c86b6e0b886ed2b7573b8e0 AS base

ENV OPCACHE_MEMORY_CONSUMPTION 256
ENV OPCACHE_REVALIDATE_FREQ 600
ENV PHP_MEMORY_LIMIT 1024M
ENV APACHE_START_SERVERS 10
ENV APACHE_MAX_SPARE_SERVERS 20
ENV APACHE_MAX_REQUEST_WORKERS 50

ENV COMPOSER_CACHE_DIR=/dev/null

USER 0
WORKDIR /app

RUN rpm --import https://supplychain.mariadb.com/MariaDB-Server-GPG-KEY

RUN { \
        echo '[mariadb]'; \
        echo 'name = MariaDB'; \
        echo 'baseurl = https://ftp.osuosl.org/pub/mariadb/yum/10.5/rhel/$releasever/$basearch'; \
        echo 'gpgkey=https://supplychain.mariadb.com/MariaDB-Server-GPG-KEY'; \
        echo 'gpgcheck=1'; \
} > /etc/yum.repos.d/mariadb.repo

RUN rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-9.noarch.rpm && \
    rpm -Uvh https://rpms.remirepo.net/enterprise/remi-release-9.rpm && \
    microdnf module enable -y php:remi-8.1 && \
    microdnf install -y MariaDB-client \
    git \
    procps \
    tar \
    tzdata \
    httpd \
    patch \
    php \
    php-apcu \
    php-cli \
    php-gd \
    php-gmp \
    php-intl \
    php-odbc \
    php-pdo \
    php-bcmath \
    php-json \
    php-mbstring \
    php-mysqlnd \
    php-xml \
    php-xmlrpc \
    php-pgsql \
    php-soap \
    php-pecl-apcu \
    php-opcache \
    php-pecl-zip \
    findutils && \
    microdnf update -y tzdata && \
    microdnf reinstall tzdata -y && \
    microdnf clean -y all

# Install composer.
RUN curl -o /tmp/composer-setup.php https://getcomposer.org/installer && \
    curl -o /tmp/composer-setup.sig https://composer.github.io/installer.sig && \
    php -r "if (hash('SHA384', file_get_contents('/tmp/composer-setup.php')) !== trim(file_get_contents('/tmp/composer-setup.sig'))) { unlink('/tmp/composer-setup.php'); echo 'Invalid installer' . PHP_EOL; exit(1); }" && \
    php /tmp/composer-setup.php --filename composer --install-dir /usr/local/bin

# Set recommended opcache settings
# see https://secure.php.net/manual/en/opcache.installation.php
RUN { \
    echo 'zend_extension=opcache'; \
    echo 'opcache.memory_consumption=${OPCACHE_MEMORY_CONSUMPTION}'; \
    echo 'opcache.interned_strings_buffer=16'; \
    echo 'opcache.max_accelerated_files=4000'; \
    echo 'opcache.revalidate_freq=60'; \
    echo 'opcache.fast_shutdown=1'; \
    echo 'opcache.enable_cli=1'; \
} > /etc/php.d/10-opcache.ini

RUN { \
    echo 'expose_php=Off'; \
    echo 'memory_limit=${PHP_MEMORY_LIMIT}'; \
    echo 'post_max_size=10M'; \
    echo 'upload_max_filesize = 10M'; \
} > /etc/php.d/20-php-sec-defaults.ini

#Read XFF headers, note this is insecure if you are not sanitizing
#XFF in front of the container
RUN { \
    echo '<IfModule mod_remoteip.c>'; \
    echo '  RemoteIPHeader True-Client-IP'; \
    echo '</IfModule>'; \
} > /etc/httpd/conf.d/remoteip.conf

#Correctly set SSL if we are terminated by it
RUN { \
    echo 'SetEnvIf X-Forwarded-Proto "https" HTTPS=on'; \
} > /etc/httpd/conf.d/remote_ssl.conf

#Setup security defaults
RUN { \
    echo 'ServerTokens Prod'; \
    echo 'ServerSignature Off'; \
    echo 'TraceEnable Off'; \
} > /etc/httpd/conf.d/security.conf

#Configure mod_deflate for css/js
RUN { \
    echo '<IfModule mod_deflate.c>'; \
    echo '     <IfModule mod_filter.c>'; \
    echo '             AddOutputFilterByType DEFLATE text/html text/plain text/xml text/css'; \
    echo '             AddOutputFilterByType DEFLATE application/x-javascript application/javascript application/ecmascript'; \
    echo '             AddOutputFilterByType DEFLATE application/rss+xml'; \
    echo '             AddOutputFilterByType DEFLATE application/xml'; \
    echo '     </IfModule>'; \
    echo '</IfModule>'; \
 } > /etc/httpd/conf.d/deflate.conf

#Enable prefork for mod_php
RUN { \
    echo 'LoadModule mpm_prefork_module modules/mod_mpm_prefork.so'; \
    echo '<IfModule mpm_prefork_module>'; \
    echo '  StartServers ${APACHE_START_SERVERS}'; \
    echo '  MinSpareServers ${APACHE_START_SERVERS}'; \
    echo '  MaxSpareServers ${APACHE_MAX_SPARE_SERVERS}'; \
    echo '  MaxRequestWorkers ${APACHE_MAX_REQUEST_WORKERS}'; \
    echo '  MaxConnectionsPerChild 2000'; \
    echo '</IfModule>'; \
} > /etc/httpd/conf.modules.d/00-mpm.conf

RUN sed -i 's/Listen 80/Listen 8080/' /etc/httpd/conf/httpd.conf && \
    chgrp -R 0 /var/log /var/run /run/httpd && \
    chmod -R g=u  /var/log /var/run /run/httpd

RUN { \
    echo '<VirtualHost *:8080>'; \
    echo 'DocumentRoot /app/web'; \
    echo '<Directory /app/web >'; \
    echo '    AllowOverride All'; \
    echo '    Require all granted';\
    echo '</Directory>'; \
    echo 'ErrorLog /dev/stderr'; \
    echo 'CustomLog /dev/stdout combined'; \
    echo '</VirtualHost>';\
} > /etc/httpd/conf.d/vhost.conf

USER 1001

EXPOSE 8080

CMD ["httpd", "-D", "FOREGROUND"]
